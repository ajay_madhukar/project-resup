Vue.component('res-skills',{
	template:'#resSkills',

	data:function()
	{
		return {
			skill:'', 

			val:false, 
		}; 
	}, 

	methods:{
		addSkill:function()
		{
			if (this.validate())
			{
				console.log("pushing skill to parent"); 
				this.$dispatch('new-skill',this.skill); 
				
				this.val = false; 

				this.skill='';

				
			}else if(!this.validate())
			{
				this.val = true; 	
			}

			
		}, 

		validate:function()
		{
			return this.skill != '' ? true : false; 
		}
	},

	ready:function()
	{
		console.log("res skills component is working"); 
	}
});
Vue.component('res-projects',{
	template:'#resProjects',

	data:function(){
		return {
			
			project:{

				title: '',

				description:''
			},

			val:false,
		}; 
	}, 

	methods:{
		addProject:function()
		{
			if (this.validate()) 
			{
				console.log("pushing project to parent"); 
				this.$dispatch('new-project',this.project); 
				this.project={};
				this.val = false; 
			}else if(!this.validate())
			{
				this.val = true; 
			} 
			
		}, 

		validate:function()
		{
			if (Object.keys(this.project).length===0) 
			{
				return false; 
			}else if(this.project.title==='' && this.project.description === '')
			{
				return false; 
			}

			return true; 
		}
	},

	ready:function()
	{
		console.log("res projects component is working"); 
	}
});
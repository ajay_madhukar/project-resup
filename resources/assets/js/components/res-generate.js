Vue.component('res-generate',{

	template:'#resGenerate', 

	props:['app_data'],

	methods:{

		checkBasicInfo:function()
		{
			console.log(Object.keys(this.app_data.basic_info).length==0); 

			return Object.keys(this.app_data.basic_info).length==0 ? true : false; 
		}, 

		//only display if variables aren't empty
		checkIfGenerateReady:function()
		{
			if(this.checkBasicInfo()==true && 
				this.app_data.summary=="" && 
				this.app_data.experiences.length==0 &&
				this.app_data.projects.length==0 && 
				this.app_data.qualifications.length==0 &&
				this.app_data.skills.length==0 
				)
			{
				return false; 
			}
			
			return true;
		},

	},

	ready:function()
	{
		console.log("res generate component is ready"); 
		console.log(this.app_data); 
	}
}); 
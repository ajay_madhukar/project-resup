Vue.component('res-education',{
	template:'#resEducation',

	data:function()
	{
		return{

			qualification:{

				name:'',

				institution_name:'',

				institution_location:'',

				graduation_date:'',

				cgpa:''

			},

			val: false, 
		};
	},

	methods:{
		addQualification:function()
		{
			if (this.validate()) 
			{	
				console.log("pushing qualification to parent"); 
				this.$dispatch('new-qualification',this.qualification); 
				this.qualification={};
				this.val=false;
			}else if(!this.validate())
			{
				this.val=true; 
			} 
		}, 

		validate:function()
		{
			if(Object.keys(this.qualification).length===0)
			{
				return false;
			}
			else if(this.qualification.name==='' && this.qualification.institution_name=== '')
			{
				return false; 
			}

			return true; 
		}
	},

	ready:function()
	{
		console.log("res education component is working"); 
	}
});
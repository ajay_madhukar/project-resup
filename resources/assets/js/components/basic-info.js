Vue.component('basic-info',{
	template:'#basicInfo',

	data:function()
	{
		return {
			basic_info:{
				full_name:'',

				phone_number:'',

				twitter_handle:'',

				personal_website:'', 

				email_address:'', 

				linkedin_url:'',

				skype_username:'',

				
			}, 

			val:false
		}; 
	},

	methods:{
		storeBasicInfo:function()
		{
			if (this.validate())
			{
				console.log("sending basic_info to parent");

				this.$dispatch('new-basic-info',this.basic_info); 

				this.basic_info={};

				this.val = false; 
			} 

			this.val = true;  
		}, 

		validate:function()
		{
			return this.basic_info.full_name != '' ? true:false; 
		}
	},

	ready:function()
	{
		console.log('basic-info component is ready'); 
	}
}); 
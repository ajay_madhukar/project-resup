Vue.component('res-experience',{
	template:'#resExperience',

	data:function()
	{
		return {

			experience:{
				
				company_name:'',

				company_location:'',

				duration:'',

				role:'',

				responsibilities:''
			},

			val:false, 
		}; 
	}, 

	methods:{
		addExperience:function()
		{
			if (this.validate()) 
			{
				console.log("pushing experience to parent"); 
				this.$dispatch('new-experience',this.experience); 
				this.experience={};
				this.val = false; 
			}else if(!this.validate())
			{
				this.val = true; 
			} 
		}, 

		validate:function()
		{
			if (Object.keys(this.experience).length===0) 
			{
				return false; 
			}else if(this.experience.company_name==='' && this.experience.role === '')
			{
				return false; 
			}

			return true; 
		},
	},

	ready:function()
	{
		console.log("res experience component is working"); 
	}
});
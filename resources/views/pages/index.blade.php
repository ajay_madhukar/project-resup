@extends('layout')

@section('content')
<div class="jumbotron" v-show="!currentView">
	<h2>Welcome To Project ResUp!</h2>
	<h3>Use This Platform To Build Your Resume...</h3>
	<a href="#basic-info" class="btn btn-primary" id="call-to-action">Get Started</a>

	<p>PS: <small>If you don't want something on your resume, just leave it blank...</small></p>
</div>

	<!-- <basic-info></basic-info>
	<res-summary></res-summary>
	<res-experience></res-experience>
	<res-projects></res-projects>
	<res-education></res-education>
	<res-skills></res-skills> -->
	
	<component v-if="currentView == 'res-generate'" :is="currentView" :app_data="$data"></component> 
	<component v-else :is="currentView"></component> 
	@stop
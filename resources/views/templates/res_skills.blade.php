<template id="resSkills">
	<p class="col-md-8 col-md-offset-2 alert alert-danger"
	    v-if="val">You can't add a blank skill...</p>
	<form 
	action="#" 
	method="POST" 
	class="col-md-8 col-md-offset-2 res-form"
	@submit.prevent>
		<div class="col-md-4">
			<res-display-list title="Your Skills"></res-display-list>
		</div>

		@include('partials.forms._res-skills')
	</form>
</template>
<template id="resProjects">
	<p class="col-md-8 col-md-offset-2 alert alert-danger"
	    v-if="val">You need to add both the name of the title and the description...</p>
	<form 
	action="#" 
	method="POST" 
	class="col-md-8 col-md-offset-2 res-form"
	@submit.prevent>

		<div class="col-md-4">
			<res-display-list title="Your Projects"></res-display-list>
		</div>

		@include('partials.forms._res-projects')
	</form>
</template>
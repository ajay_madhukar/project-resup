<template id="basicInfo">
	<p class="col-md-8 col-md-offset-2 alert alert-danger"
	    v-if="val">At least fill in your name...</p>
	<form action="#" 
		method="POST" 
		class="col-md-8 col-md-offset-2 res-form"
		@submit.prevent>
		@include('partials.forms._basic-info')
	</form>
</template>